from sqlalchemy import Column, MetaData, String, Table, inspect
from sqlalchemy.exc import IntegrityError
from utils import get_engine
from flask import Flask

from test import test_bp


def create_app():
    app = Flask(__name__)

    engine = get_engine()
    if not inspect(engine).has_table("test"):
        meta = MetaData()
        Table(
            "test",
            meta,
            Column("test", String, nullable=False, unique=True),
        )
        meta.create_all(engine)

    blueprints = [test_bp]

    for bp in blueprints:
        app.register_blueprint(bp)

    return app


app = create_app()
app.run()
