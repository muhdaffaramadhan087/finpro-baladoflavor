from utils import get_engine, run_query
from flask import Blueprint, request
from sqlalchemy import (
    MetaData,
    Table,
    delete,
    insert,
    select,
)
from sqlalchemy.exc import IntegrityError


test_bp = Blueprint("book", __name__, url_prefix="/test")


@test_bp.route("", methods=["POST"])
def add_test():
        return {"mesagge": "testing"}, 200