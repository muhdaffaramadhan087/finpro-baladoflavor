FROM python:3.9-slim-buster

COPY . .

RUN pip3 install -r requirements.txt

EXPOSE 5000

WORKDIR /app

CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]